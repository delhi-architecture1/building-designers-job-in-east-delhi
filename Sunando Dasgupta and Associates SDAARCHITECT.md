At SDAARCHITECT, we strive to create institutional architecture that can inspire and enhance the well being of its visitors and users and promote an aesthetic and functional balance with the complex physical and cultural environment of modern India. 

We believe that contemporary Indian architects are charged with the responsibility of creating institutional spaces that complement their site and context, and thoroughly fulfill their functional purpose while upholding a dedication to environmental, social, and community concerns of a uniquely Indian nature. 

As architects of a rapidly changing urban Indian landscape, we must not only study the way buildings are built, but also how space is and will be used. A core tenet of SDAARCHITECT's design philosophy is the belief that all aspects of user experience must be considered in addition to the technological systems of construction. At SDAARCHITECT, we consider ourselves lucky to be part of the process of building our environments.
